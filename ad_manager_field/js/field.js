(function ($) {
  function unitChanged () {
    var d = Drupal.settings.adManagerField.descriptions;
    var a = $(this).siblings('.description').children('.ad-manager-field');
    if (typeof d[$(this).val()] === 'undefined') {
      a.hide();
      return;
    }

    // Set the additional description text.
    a.html(d[$(this).val()]).show();
  }

  function appendAdditional () {
    var d = $(this).siblings('.description');
    var a = $('<div class="ad-manager-field"></div>')
      .hide();
    // If the description element is present.
    if (d.length) {
      a.addClass('ad-manager-field-margin');
      a.appendTo(d);
    }
    // Else create it.
    else {
      d = $('<div class="description"></div>');
      d.appendTo($(this).parent());
      a.appendTo(d);
    }
  }

  function fixSettings (settings) {
    for (var i in settings) {
      if (!settings.hasOwnProperty(i)) {
        continue;
      }

      // drupal_add_js() thinks it is ok to take string assignments and turn
      // them into arrays from subsequent add js calls. We fix it here instead
      // of writing loads of PHP code to prevent multiple calls.
      if (typeof settings[i] === 'object') {
        settings[i] = settings[i][0];
      }
    }
  }

  Drupal.behaviors.adManagerField = {
    attach: function (context, settings) {
      fixSettings(settings.adManagerField.descriptions);
      // Bind the unitChanged handler, set up the additional-description div,
      // and populate and additional description for the currently selected
      // item.
      $('.ad-manager-field', context)
        .once('adManagerField')
        .bind('change', unitChanged)
        .each(appendAdditional)
        .each(unitChanged);
    }
  };
})(jQuery);
